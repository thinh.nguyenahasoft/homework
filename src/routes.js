import Vue from 'vue'
import VueRouter from 'vue-router'
import LoginForm from './components/LoginForm/LoginForm.vue';
import ClientPage from './components/ClientPage/ClientPage.vue';

Vue.use(VueRouter)

export const router = new VueRouter({
    routes: [
        {path: '/', component: LoginForm},
        {path: '/client', component: ClientPage}
    ]
})